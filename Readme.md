# example-service
## Description
   1. Example-service is a stateless micro service which implements REST APIs to manage the basic user directory information

   2. It is built using Python Flask framework, provides API to perform CRUD (Create, Read, Update and Delete) Operations

   3. More information can be found at https://bitbucket.org/sripathi2610/example-service/src/master/

## Technology Stack Used

   1. Terraform

   2. Helm Charts

   3. Kubernetes on GCP cloud

   4. Concourse CI

   5. SonarQube

   6. Docker

   7. Prometheus

## Installation / Usage Instructions

   This repository has following

   1. To setup infra - [link](https://bitbucket.org/sripathi2610/example-service/src/master/infra/)

   2. To setup CI jobs - [link](https://bitbucket.org/sripathi2610/example-service/src/master/ci/)

   3. To setup Helm chart deployments - [link](https://bitbucket.org/sripathi2610/example-service/src/master/helm-chart/)

